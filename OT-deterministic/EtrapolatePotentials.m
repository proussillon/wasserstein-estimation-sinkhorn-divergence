function [F,G] = EtrapolatePotentials(a,b,f,g,t,t0,epsilon)

% use soft c-transform to extrapolate (f,g) on t to (F,G) on t0

a = a(:); 
b = b(:)';
f = f(:); g = g(:)';

mina = @(H,epsilon)-epsilon*log( sum(a .* exp(-H/epsilon),1) );
minb = @(H,epsilon)-epsilon*log( sum(b .* exp(-H/epsilon),2) );
mina = @(H,epsilon)mina(H-min(H,[],1),epsilon) + min(H,[],1);
minb = @(H,epsilon)minb(H-min(H,[],2),epsilon) + min(H,[],2);
C1 = abs( PerTorus( t0(:)-t(:)' ) ).^2;

G = mina(C1'-f,epsilon);
F = minb(C1-g,epsilon);


end