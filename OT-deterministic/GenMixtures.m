function [a,b] = GenMixtures(name, n)


t = (0:n-1)/n;
normalize = @(x)x/sum(x);

% mixture of Gaussians
G = @(m,sigma)exp( -PerTorus(t(:)-m).^2/(2*sigma^2) );

switch name
    case 'single'
        a = G(.3,.05);
        b = G(.5,.07);
    case 'twomixtures'
        a = G(.5,.08);
        b = G(.2,.05) + .6*G(.7,.1);
    case 'threemixtures'
        a = G(.5,.1);
        a = G(.2,.05) + .6*G(.7,.1);
%        b = G(.2,.05) + .6*G(.55,.05) + .8*G(.8,.05);
        b = G(.1,.04) + .55*G(.37,.03) + .65*G(.55,.03) + .8*G(.9,.04);
end


vmin = .03; % minimum mass --> increase to regularize
a = normalize(a+vmin);
b = normalize(b+vmin);

end