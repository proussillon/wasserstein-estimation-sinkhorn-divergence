vmax = max([remean(f0); remean(g0)]); vmin = min([remean(f0); remean(g0)]);
myplot = @(f, col)plot(t0, remean(f), 'color', col, 'LineWidth', 2);
myplotx = @(f, col)plot(t0, remean(f), '--', 'color', col, 'LineWidth', 2);


myrange = @(u,v,q,n)u + (v-u)*linspace(0,1,n).^q;



nlist = 2.^(2:10)';
nlist = round( 2.^(2:.5:10)' );
q_n = length(nlist);
q_eps = 20;
eps_list = ( 10.^myrange(-1,-3,1,q_eps) ).^2;


ErrA  = zeros(q_eps, q_n);
ErrB  = zeros(q_eps, q_n);
ErrAd = zeros(q_eps, q_n);  % debiased
ErrBd = zeros(q_eps, q_n);
ErrAe = zeros(q_eps, q_n);  % extrapolated
ErrBe = zeros(q_eps, q_n);
ErrAd1 = zeros(q_eps, q_n);  % wrong
ErrBd1 = zeros(q_eps, q_n);
mynorm = @(x)sum(abs(x(:)));
ErrorA = @(s,t)mynorm( a0 .* (remean(s(:))-remean(t(:)))  );
ErrorB = @(s,t)mynorm( b0 .* (remean(s(:))-remean(t(:)))  );
nshift = 4; % number of shift of the grid


options.niter = 5000;
options.tol = 1e-10;
%
options.niter = 3000;
options.tol = 1e-8;
options.verb = 0;
for i_n=1:q_n
    n = nlist(i_n); 
    fprintf('------------ n=%d ------------\n', n);
    for i_shift=1:nshift
        t = (0:n-1)'/n + (i_shift-1)/nshift*1/n;
        [a,aquant] = SubSampleDensity(a0,t0,t);
        [b,bquant] = SubSampleDensity(b0,t0,t);
        C = TorusCost(t,t);
        f = zeros(n,2); g = zeros(n,2);  faa = zeros(n,2); gbb = zeros(n,2);
        F = []; G = []; Faa = []; Gbb = [];
        for i_eps=1:q_eps
            progressbar(i_eps,q_eps);
            epsilon = eps_list(i_eps);
            for m=1:2
                myeps = epsilon*sqrt(2)^(m-1);
                options.f = f(:,m);
                [gammaS, f(:,m), g(:,m), Err] = sinkhorn(C, a, b, myeps, options);
                options.f = faa(:,m);
                [~, faa(:,m), Err] = sinkhorn_sym(C, a, myeps, options);
                options.f = gbb(:,m);
                [~, gbb(:,m), Err] = sinkhorn_sym(C, b, myeps, options);
                % extrapolate the potential to high res grid
                [F(:,m),G(:,m)]     = EtrapolatePotentials(a,b,f(:,m),g(:,m),t,t0,myeps);
                Faa(:,m) = EtrapolatePotentials(a,a,faa(:,m),faa(:,m),t,t0,myeps);
                Gbb(:,m) = EtrapolatePotentials(b,b,gbb(:,m),gbb(:,m),t,t0,myeps);
            end
            % debiased and extrapolted poltentials
            Fd = F-Faa; Gd = G-Gbb;
            Fe = 2*Fd(:,1) - Fd(:,2); Ge = 2*Gd(:,1)-Gd(:,2);   % 2*S_lambda-{S_sqrt{2}lambda}
            % compute errors for basics
            ErrA(i_eps,i_n)   =  ErrA(i_eps,i_n)  + ErrorA(f0,F(:,1))/nshift;
            ErrB(i_eps,i_n)   =  ErrB(i_eps,i_n)  + ErrorB(g0,G(:,1))/nshift;
            ErrAd(i_eps,i_n)  =  ErrAd(i_eps,i_n) + ErrorA(f0,Fd(:,1))/nshift;
            ErrBd(i_eps,i_n)  =  ErrBd(i_eps,i_n) + ErrorB(g0,Gd(:,1))/nshift;
            ErrAe(i_eps,i_n)  =  ErrAe(i_eps,i_n) + ErrorA(f0,Fe)/nshift;
            ErrBe(i_eps,i_n)  =  ErrBe(i_eps,i_n) + ErrorB(g0,Ge)/nshift;
        end
        % display
        clf; hold on;
        myplot(F(:,1), 'b'); myplot(Fd(:,1), [.3 .3 1]); myplot(Fe, [.6 .6 1]); myplotx(f0, 'b');
        myplot(G(:,1), 'r'); myplot(Gd(:,1), [1 .3 .3]); myplot(Ge, [1 .6 .6]);  myplotx(g0, 'r');
        box on;  axis([0, 1, 1.3*vmin, 1.3*vmax]);
        set(gca, 'XTick', [], 'YTick', [], 'PlotBoxAspectRatio', [1 2/3 1]);
        drawnow;
    end
end

lw = 2;
my_ilist = [1 5]; col = {[1 0 0] [0 0 1] [.5 0 .5]};
clf; 
for it=1:length(my_ilist)
    i=my_ilist(it);
    loglog(nlist, ErrA(i,:), 'color', col{it}, 'LineWidth', lw ); hold on;
    loglog(nlist, ErrAd(i,:), '--', 'color', col{it}, 'LineWidth', lw );
    loglog(nlist, ErrAe(i,:), ':', 'color', col{it}, 'LineWidth', lw );
end
thresh = @(x)x.*(x>3.4e-5);
loglog(nlist, thresh(min(ErrA)), 'k-', 'LineWidth', 2 );
loglog(nlist, thresh(min(ErrAd)), 'k--', 'LineWidth', 2 );
loglog(nlist, thresh(min(ErrAe)), 'k:', 'LineWidth', 2 );
axis tight; box on; grid on;
set(gca, 'PlotBoxAspectRatio', [1 2/3 1]);
xlabel('log(n)'); ylabel('log(Error)');
saveas(gcf, [rep 'Error_n.png']);
saveas(gcf, [rep 'Error_n.eps'], 'epsc');

my_ilist = [4 5 7]; % for step 1
my_ilist = [4 7 9]; % for step  
col = {[0 .7 0] [.5 0 .5] [.9 .6 0]};
clf; 
for it=1:length(my_ilist)
    i=my_ilist(it); 
    loglog(1./eps_list, ErrA(:,i), 'color', col{it}, 'LineWidth', 2 ); hold on;
    loglog(1./eps_list, ErrAd(:,i), '--', 'color', col{it}, 'LineWidth', 2 );
    loglog(1./eps_list, ErrAe(:,i), ':', 'color', col{it}, 'LineWidth', 2 );
end
loglog(1./(eps_list), min(ErrA'), 'k-', 'LineWidth', 2 );
loglog(1./(eps_list), min(ErrAd'), 'k--', 'LineWidth', 2 );
loglog(1./(eps_list), min(ErrAe'), 'k:', 'LineWidth', 2 );
axis tight; box on; grid on;
axis([min(1./(eps_list)), 1.5e4, 5.6175e-05, 0.0047])
set(gca, 'PlotBoxAspectRatio', [1 2/3 1]);
xlabel('1/\epsilon'); ylabel('Error');
saveas(gcf, [rep 'Error_epsilon.png']);
saveas(gcf, [rep 'Error_epsilon.eps'], 'epsc');

% plot optimal eps for each n
sm = @(x)x;
[ErrA_min,I] = min(ErrA, [], 1); ErrA_argmin = eps_list(I);
[ErrAd_min,I] = min(ErrAd, [], 1); ErrAd_argmin = eps_list(I);
[ErrAe_min,I] = min(ErrAe, [], 1); ErrAe_argmin = eps_list(I);
clf; 
loglog(nlist, sm(ErrA_argmin), 'k', 'LineWidth', 2); hold on;
loglog(nlist, sm(ErrAe_argmin), 'k:', 'LineWidth', 2);
loglog(nlist, sm(ErrAd_argmin), 'k--', 'LineWidth', 2);
axis tight; box on; grid on; 
set(gca, 'PlotBoxAspectRatio', [1 2/3 1]);
xlabel('n'); ylabel('\epsilon_{opt}(n)');
saveas(gcf, [rep 'epsilon_opt.png']);
saveas(gcf, [rep 'epsilon_opt.eps'], 'epsc');


