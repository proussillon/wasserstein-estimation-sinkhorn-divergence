vmax = max([remean(f0); remean(g0)]); vmin = min([remean(f0); remean(g0)]);
myplot = @(f, st, col)plot(t0, remean(f), st, 'color', col, 'LineWidth', 2);
myplotx = @(f, st, col)plot(t0, remean(f), st, 'color', col, 'LineWidth', 2);

mysave = @(name, it)saveas(gcf, [rep name '-' znum2str(it,3) '.png']);
mysave = @(name, it)saveas(gcf, [rep name '-' znum2str(it,3) '.eps'], 'epsc');

q = 50;
epsilon = (.08)^2;
epsilon = (.05)^2;
nlist = round( 5 + 100*linspace(0,1,q).^3 );
nlist = unique(nlist); 
%
nlist =  [8 16 32 128];
q = length(nlist);
options.tol = 1e-20;
options.verb = 0;
for it=1:q
    n = nlist(it); t = (0:n-1)/n;
    [a,aquant] = SubSampleDensity(a0,t0,t);
    [b,bquant] = SubSampleDensity(b0,t0,t);
    %% Display inputs
    clf; hold on;
    area(t0,aquant*n, 'FaceColor', 'b', 'EdgeColor', 'b'); alpha(.5);
    area(t0,bquant*n, 'FaceColor', 'r', 'EdgeColor', 'r'); alpha(.5);
    % plot(t,a*n,'b.','MarkerSize', 10);
    % plot(t,b*n,'r.','MarkerSize', 10);
    box on;
    axis([0 1 0 n*max([a; b])*1.03]);
    set(gca, 'XTick', [], 'YTick', [], 'PlotBoxAspectRatio', [1 1/2 1]);
    mysave('quantdens', it);
    %% Sinkhorn 
    C = TorusCost(t,t);
    options.f = []; 
    [gammaS, f, g, Err] = sinkhorn(C, a, b, epsilon, options);
    [~, faa, gaa, Err] = sinkhorn(C, a, a, epsilon, options);
    [~, fbb, gbb, Err] = sinkhorn(C, b, b, epsilon, options);
    % extrapolate the potential to high res grid
    [F,G] = EtrapolatePotentials(a,b,f,g,t,t0,epsilon);
    [Faa,~] = EtrapolatePotentials(a,a,faa,gaa,t,t0,epsilon);
    [~,Gbb] = EtrapolatePotentials(b,b,fbb,gbb,t,t0,epsilon);
    %
    clf; hold on;
    myplot(F, '-', 'b'); myplot(F-Faa/2, ':', 'b'); myplotx(f0, '-',[0 0 .5]);
    myplot(G, '-', 'r'); myplot(G-Gbb/2, ':', 'r'); myplotx(g0, '-', [.5 0 0]);
    box on;
    axis([0, 1, 1.1*vmin, 1.1*vmax]);
    set(gca, 'XTick', [], 'YTick', [], 'PlotBoxAspectRatio', [1 1/2 1]);
    drawnow;
    mysave('anim-n', it);
end