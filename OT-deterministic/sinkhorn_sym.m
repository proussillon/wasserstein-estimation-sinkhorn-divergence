function [P,f,Err] = sinkhorn_sym(C,a,epsilon,options)

a = a(:); 
n = length(a);

mina = @(H,epsilon)-epsilon*log( sum(a .* exp(-H/epsilon),1) );
mina = @(H,epsilon)mina(H-min(H,[],1),epsilon) + min(H,[],1);

niter = getoptions(options, 'niter', 1000);
tol = getoptions(options, 'tol', 0);
verb = getoptions(options, 'verb', 1);

f = getoptions(options, 'f', []);
if isempty(f)
    f = zeros(n,1);
end
Err = [];
for it=1:niter
    if verb
    progressbar(it,niter);
    end
    f = f/2 + mina(C-f,epsilon)'/2;
    % generate the coupling
    P = a .* exp((f+f'-C)/epsilon) .* a';
    % check conservation of mass
    Err(it) = norm(sum(P,2)-a,1); 
    if Err(end)<tol
        break
    end
end

end