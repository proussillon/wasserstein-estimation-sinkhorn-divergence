
vmax = max([remean(f0); remean(g0)]); vmin = min([remean(f0); remean(g0)]);
myplot = @(f, st, col)plot(t0, remean(f), st, 'color', col, 'LineWidth', 2);
myplotx = @(f, st, col)plot(t0, remean(f), st, 'color', col, 'LineWidth', 2);

mysave = @(name, it)saveas(gcf, [rep name '-' znum2str(it,3) '.png']);
mysave = @(name, it)saveas(gcf, [rep name '-' znum2str(it,3) '.eps'], 'epsc');

q = 8;
eps_list = ( 10.^( linspace(.5,-1.5,q) ) ).^2;
eps_list = ( [.25 .1 .075 .05] ).^2;
q = length(eps_list);
n = 100; t = (0:n-1)/n;
f = zeros(n,1); faa = zeros(n,1); fbb = zeros(n,1);
C = TorusCost(t,t);
[a,aquant] = SubSampleDensity(a0,t0,t);
[b,bquant] = SubSampleDensity(b0,t0,t);
options.tol = 1e-10;
for it=1:q
    epsilon = eps_list(it);
    options.f = f; 
    [gammaS, f, g, Err] = sinkhorn(C, a, b, epsilon, options);
    options.f = faa; 
    [~, faa, gaa, ~] = sinkhorn(C, a, a, epsilon, options);
    options.f = faa; 
    [~, fbb, gbb, ~] = sinkhorn(C, b, b, epsilon, options);
    % extrapolate the potential to high res grid
    [F,G] = EtrapolatePotentials(a,b,f,g,t,t0,epsilon);
    [Faa,~] = EtrapolatePotentials(a,a,faa,gaa,t,t0,epsilon);
    [~,Gbb] = EtrapolatePotentials(b,b,fbb,gbb,t,t0,epsilon);
    %
    clf; hold on;
    myplot(F, '-', 'b'); myplot(F-Faa/2, ':', 'b'); myplotx(f0, '-',[0 0 .5]);
    myplot(G, '-', 'r'); myplot(G-Gbb/2, ':', 'r'); myplotx(g0, '-', [.5 0 0]);
    axis([0, 1, 1.1*vmin, 1.1*vmax]);
    box on; 
    set(gca, 'XTick', [], 'YTick', [], 'PlotBoxAspectRatio', [1 1/2 1]);
    mysave('anim-eps', it);
end