function [f0,g0,Tab,Tba] = OptimalTransport1D(a1,b1)

% Solve OT in 1D on a *periodic *domain

n1 = length(a1);
t0 = (0:n1)'/n1;
rev = @(x)interp1(x,t0,t0,'linear','extrap'); % inversion
quantile = @(a)rev([0;cumsum(a)]);
qa = quantile(a1);
d = zeros(n1,1);
for i=1:n1
    a0 = circshift(a1,i-1);
    b0 = circshift(b1,i-1);
	d(i) = norm( quantile(a0)-quantile(b0) )^2;        
end
[~,i] = min(d);  delta = (i-1)/n1;
% so use usual formula between a and b0
a0 = circshift(a1,i-1);
b0 = circshift(b1,i-1);
% T_{a->b} : a -[cum(a)]->U-[quantile(b0)]->b0
Tab = interp1(t0, quantile(b0), [0;cumsum(a0)],'linear','extrap');
Tba = interp1(t0, quantile(a0), [0;cumsum(b0)],'linear','extrap');
% retrieve potentials 
f0 = -2*cumsum(Tab - t0)/n1; f0 = circshift(f0,1-i);
g0 = -2*cumsum(Tba - t0)/n1; g0 = circshift(g0,1-i);

rest = @(x)x(1:end-1); % remove last point
f0 = rest(f0); 
g0 = rest(g0); 
Tab = rest(Tab);
Tba = rest(Tba);

end