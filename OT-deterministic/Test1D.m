addpath('./toolbox/');
rep = 'results/';
[~,~] = mkdir(rep);

remean = @(x)x-mean(x);

% geodesic distance
name = 'single';
name = 'twomixtures';
name = 'threemixtures';

% just a simple display to start
n = 500;
t = (0:n-1)/n;
[a,b] = GenMixtures(name, n);
clf; hold on;
area(t,a*n, 'FaceColor', 'b', 'EdgeColor', 'b'); alpha(.5);
area(t,b*n, 'FaceColor', 'r', 'EdgeColor', 'r'); alpha(.5);
box on;
axis([0 1 0 n*max([a; b])*1.03]);
set(gca, 'XTick', [], 'YTick', [], 'PlotBoxAspectRatio', [1 2/3 1]);
saveas(gcf, [rep 'densities.png']);


% compute "exact" OT using integrals 
n0 = 4000; t0 = (0:n0-1)'/n0; 
[a0,b0] = GenMixtures(name, n0);
[f0,g0,Tab,Tba] = OptimalTransport1D(a0,b0);

% options for sinkhorn
clear options;
options.niter = 3000;
options.tol = 1e-10;

% batch_epsilon;
% batch_n;
batch_n_epsilon;