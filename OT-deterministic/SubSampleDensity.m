function [a,aquant] = SubSampleDensity(a0,t0,t)

n0 = length(a0);
[~,I] = min( abs(PerTorus(t(:)-t0(:)')) );
U = sparse(I(:), (1:n0)', ones(n0,1) );
a = (U*a0) ./ sum(U,2);
aquant = U'*a;


end