function C = TorusCost(s,t)

C = PerTorus( s(:)-t(:)' );
% C(C>1/2) = C(C>1/2)-1;
% C(C<-1/2) = C(C<-1/2)+1;
C = abs(C).^2;

end
