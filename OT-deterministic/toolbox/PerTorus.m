function C = PerTorus(C)

C = mod(C,1);
C(C>1/2) = C(C>1/2)-1;

end